#include <stdio.h>

#include "add.h"

int main()
{
	printf("Sum of 5 and 7 is: %i\n", add(5, 7));

	return(0);
}
