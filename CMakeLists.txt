cmake_minimum_required(VERSION 3.20)

project(static-library LANGUAGES C)

add_library(Add add.c)

add_executable(${PROJECT_NAME} ${PROJECT_NAME}.c)

target_include_directories(${PROJECT_NAME} INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(${PROJECT_NAME} PRIVATE Add)
